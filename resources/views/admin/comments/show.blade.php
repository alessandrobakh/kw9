@extends('layouts.admin')

@section('content')

    <div>
        User name: {{$comment->user->name}}
        <br>
        Comment: {{$comment->body}}
        <br>
        Score: {{$comment->score}}
    </div>
    <br>

    <a href="{{route('admin.comments.index')}}">Back</a>

@endsection
