@extends('layouts.admin')

@section('content')

    <h1>All Comments:</h1>

    @if($comments->count()>0)

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">User</th>
                <th scope="col">Comment</th>
                <th scope="col">Score</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($comments as $comment)
            <tr>
                <th scope="row">{{$comment->id}}</th>
                <td>{{$comment->user->name}}</td>
                <td>
                    <a href="{{route('admin.comments.show', ['comment' => $comment])}}">{{$comment->body}}</a>
                </td>
                <td>{{$comment->score}}</td>
                <td>
                    <form action="{{route('admin.comments.destroy', ['comment' => $comment])}}" method="post">
                        @csrf
                        @method('delete')
                        <button class="btn btn-outline-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

    @else
    <p>No comments</p>

    @endif

@endsection
