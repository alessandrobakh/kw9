@extends('layouts.admin')

@section('content')

    <h1>Page of user: {{$user->name}}</h1>

    <p>{{$user->email}}</p>

    @if($user->images->count()>0)

        <table class="table">
            <thead>
            <tr>
                <th scope="col">Image</th>
                <th scope="col">Name</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($user->images as $image)
                <tr>
                    <td>
                        <img width="100px" height="100px" src="{{asset('/storage/' . $image->image)}}" alt="{{asset('/storage/' . $image->image)}}">
                    </td>
                    <td>{{$image->name}}</td>
                    <td>
                        <a href="{{route('admin.images.edit', ['image' => $image])}}"><span class="btn btn-outline-warning">Edit</span></a>
                        <form action="{{route('admin.images.destroy', ['image' => $image])}}" method="post">
                            @csrf
                            @method('delete')
                            <button class="btn btn-outline-danger" type="submit">Delete</button>
                        </form>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    @else
        <p>No images</p>
    @endif

@endsection
