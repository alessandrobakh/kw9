@extends('layouts.admin')

@section('content')

    <h1>{{$image->name}}</h1>
    <br>

    <img style="max-height: 1000px; max-width: 1000px" src="{{asset('/storage/'. $image->image)}}" alt="{{asset('/storage/'. $image->image)}}">

    <br>
    <p>Average score: {{round($image->comments->pluck('score')->avg(), 2)}}</p>

    <h3>Comments: </h3>
    <br>

    <br>

    <div class="all-comments">
        @foreach($comments as $comment)

            <div id="comment-{{$comment->id}}" class="border mb-1 p-1">
                <h5>{{$comment->user->name}} score: {{$comment->score}}</h5>
                <p>{{$comment->body}}</p>
                <div>
                    <form action="{{route('admin.comments.destroy', ['comment' => $comment])}}" method="post">
                        @csrf
                        @method('delete')
                        <button class="btn btn-outline-danger" type="submit">Delete</button>
                    </form>
                </div>
            </div>

        @endforeach
    </div>

@endsection
