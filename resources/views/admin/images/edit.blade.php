@extends('layouts.client')

@section('content')

    <form enctype="multipart/form-data" action="{{route('admin.images.update', ['image' => $image])}}" method="post">
        @csrf
        @method('put')

        <input type="hidden" name="user_id" value="{{$image->user->id}}">

        <div class="row mb-3">
            <label for="name" class="col-form-label">Name</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="name" name="name" value="{{$image->name}}">
            </div>
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <img src="{{asset('/storage/' . $image->image)}}" alt="{{$image->image}}" style="width:100px;height:100px;"><br/><br>

        <div class="row mb-3">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="image" name="image">
                <label class="custom-file-label" for="image">Choose image</label>
            </div>
            @error('image')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <br>
        <button type="submit" class="btn btn-primary">Save image</button>
    </form>

@endsection
