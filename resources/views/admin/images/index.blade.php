@extends('layouts.admin')

@section('content')

    <h1>All images:</h1>
    <a href="{{route('admin.images.create')}}">Create new image</a>

    @if($images->count()>0)

        <div class="row row-cols-1 row-cols-md-3 g-4">
            @foreach($images as $image)
                <div class="col mt-2">
                    <div class="card">
                        <img src="{{asset('/storage/' . $image->image)}}" class="card-img-top" alt="{{asset('/storage/' . $image->image)}}">
                        <div class="card-body">
                            <h5 class="card-title">
                                <a href="{{route('admin.images.show', ['image' => $image])}}">{{$image->name}}</a>
                            </h5>
                            <p class="card-text">By:
                                <a href="{{route('admin.users.show', ['user' => $image->user])}}">{{$image->user->name}}</a>
                            </p>
                            <div class="card-footer">
                                <a href="{{route('admin.images.edit', ['image' => $image])}}"><span class="btn btn-outline-warning">Edit</span></a>
                                <form action="{{route('admin.images.destroy', ['image' => $image])}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-outline-danger" type="submit">Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    @else
        <p>No images</p>

    @endif

    <div class="row justify-content-md-center p-5">

        <div class="col-md-auto">

            {{ $images->links('pagination::bootstrap-4') }}

        </div>

    </div>

@endsection
