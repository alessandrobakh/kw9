@extends('layouts.client')

@section('content')

    <h1>@lang('messages.all_images'):</h1>

    @if($images->count()>0)

        <div class="row row-cols-1 row-cols-md-3 g-4">
        @foreach($images as $image)
                <div class="col mt-2">
                    <div class="card">
                        <img src="{{asset('/storage/' . $image->image)}}" class="card-img-top" alt="{{asset('/storage/' . $image->image)}}">
                        <div class="card-body">
                            <h5 class="card-title">
                                <a href="{{route('client.images.show', ['image' => $image])}}">{{$image->name}}</a>
                            </h5>
                            <p class="card-text">@lang('messages.by'):
                                <a href="{{route('client.users.show', ['user' => $image->user])}}">{{$image->user->name}}</a>
                            </p>
                        </div>
                    </div>
                </div>
        @endforeach
        </div>

    @else
    <p>@lang('messages.no_images')</p>

    @endif

    <div class="row justify-content-md-center p-5">

        <div class="col-md-auto">

            {{ $images->links('pagination::bootstrap-4') }}

        </div>

    </div>

@endsection
