@extends('layouts.client')

@section('content')

    <form enctype="multipart/form-data" action="{{route('client.images.store')}}" method="post">
        @csrf

        <div class="row mb-3">
            <label for="name" class="col-form-label">@lang('messages.name')</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="name" name="name">
            </div>
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="row mb-3">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="image" name="image">
                <label class="custom-file-label" for="image">@lang('messages.choose_image')</label>
            </div>
            @error('image')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <br>
        <button type="submit" class="btn btn-primary">@lang('messages.create_image')</button>
    </form>

@endsection
