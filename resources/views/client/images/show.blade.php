@extends('layouts.client')

@section('content')

    <h1>{{$image->name}}</h1>
    <br>

    <img style="max-height: 1000px; max-width: 1000px" src="{{asset('/storage/'. $image->image)}}" alt="{{asset('/storage/'. $image->image)}}">

    <br>
    <p>@lang('messages.average_score'): {{round($image->comments->pluck('score')->avg(), 2)}}</p>

    <h3>@lang('messages.comments'): </h3>
    <br>

    @if(Auth::check())
    <p>
        <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">@lang('messages.create_comment')</a>
    </p>
    <div class="row">
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <div class="card card-body">
                    <form action="{{route('client.images.comments.store', ['image' => $image])}}" id="create-comment" method="post">
                        @csrf
                        <input type="hidden" id="image_id" value="{{$image->id}}">

                        <div class="form-group">
                            <label for="score">@lang('messages.your_score')</label>
                            <input min="1" max="5" step="0.5" type="number" name="score" id="score">
                        </div>
                        <div class="form-group">
                            <label for="body">@lang('messages.comment')</label>
                            <textarea name="body" class="form-control" id="body" rows="3" required></textarea>
                        </div>
                        <button id="create-comment-btn" type="submit" class="btn btn-outline-success">@lang('messages.add_new_comment')
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endif

    <br>

    <div class="all-comments">
        @foreach($comments as $comment)

            <div id="comment-{{$comment->id}}" class="border mb-1 p-1">
                <h5>{{$comment->user->name}} score: {{$comment->score}}</h5>
                <p>{{$comment->body}}</p>
            </div>

        @endforeach
    </div>

@endsection
