@extends('layouts.client')

@section('content')

    <form action="{{route('client.users.update', ['user' => $user])}}" method="post">
        @csrf
        @method('put')
        <div class="row mb-3">
            <label for="name" class="col-form-label">@lang('messages.name')</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}">
            </div>
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="row mb-3">
            <label for="email" class="col-form-label">@lang('messages.email')</label>
            <div class="col-sm-6">
                <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}">
            </div>
            @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="row mb-3">
            <label for="password" class="col-form-label">@lang('messages.password')</label>
            <div class="col-sm-6">
                <input type="password" class="form-control" id="password" name="password" value="{{$user->password}}">
            </div>
            @error('password')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <br>
        <button type="submit" class="btn btn-primary">@lang('messages.submit')</button>
    </form>

@endsection
