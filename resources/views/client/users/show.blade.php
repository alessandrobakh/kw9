@extends('layouts.client')

@section('content')

    <h1>{{$user->name}} @lang('messages.profile')</h1>
    @can('update', $user)
    <a href="{{route('client.users.edit', ['user' => $user])}}">@lang('messages.edit')</a>
    <br>
    <br>
    @endcan

    @lang('messages.images'):

    <br>
    <br>
    @can('view', $user)
    <a href="{{route('client.images.create')}}">@lang('messages.create_new_image')</a>
    <br>
    @endcan

    @if($user->images->count()>0)

        <table class="table">
            <thead>
            <tr>
                <th scope="col">@lang('messages.image')</th>
                <th scope="col">@lang('messages.name')</th>
                <th scope="col">@lang('messages.actions')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($user->images as $image)
            <tr>
                <td>
                    <img width="100px" height="100px" src="{{asset('/storage/' . $image->image)}}" alt="{{asset('/storage/' . $image->image)}}">
                    </td>
                <td>{{$image->name}}</td>
                <td>
                    @can('forceDelete', $image)
                    <form action="{{route('client.images.destroy', ['image' => $image])}}" method="post">
                        @csrf
                        @method('delete')
                        <button class="btn btn-outline-danger" type="submit">@lang('messages.remove')</button>
                    </form>
                    @endcan
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

    @else
    <p>@lang('messages.no_images')</p>
    @endif

@endsection
