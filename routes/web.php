<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\ImagesController::class, 'index'])->middleware('auth', 'language');

Route::prefix('admin')->name('admin.')->middleware('auth')->group(function (){
    Route::resource('users', \App\Http\Controllers\Admin\UsersController::class);
    Route::resource('images', \App\Http\Controllers\Admin\ImagesController::class);
    Route::resource('comments', \App\Http\Controllers\Admin\CommentsController::class)->except(['create', 'store', 'edit', 'update']);
});

Route::prefix('client')->name('client')
    ->resource('images', \App\Http\Controllers\ImagesController::class)->except(['edit', 'update'])->middleware(['auth', 'language']);
Route::prefix('client')->name('client')
    ->resource('images.comments', \App\Http\Controllers\CommentsController::class)->only(['store'])->middleware(['auth', 'language']);
Route::prefix('client')->name('client')
    ->resource('users', \App\Http\Controllers\UsersController::class)->only(['show', 'edit', 'update'])->middleware(['auth', 'language']);

Route::get('language/{locale}', [\App\Http\Controllers\LanguageSwitcherController::class, 'switcher'])
    ->name('language.switcher')->where('locale', 'en|ru');




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
