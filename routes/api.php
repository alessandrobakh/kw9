<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('users', \App\Http\Controllers\Api\UsersController::class)->only(['index']);
Route::apiResource('images', \App\Http\Controllers\Api\ImagesController::class)->only('index');
Route::apiResource('comments', \App\Http\Controllers\Api\CommentsController::class)->only('index');
