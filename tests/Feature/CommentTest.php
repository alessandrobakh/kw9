<?php

namespace Tests\Feature;

use App\Models\Comment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentTest extends TestCase
{
    /**
     * @group comments
     * @return void
     */
    public function test_comments_index()
    {
        $comments = Comment::all();
        $response = $this->getJson(route('comments.index'));
        $response->assertStatus(200);

        foreach ($comments as $comment){
            $response->assertSeeText($comment->score);
            $response->assertSeeText($comment->body);
            $response->assertSeeText($comment->user->name);
            $response->assertSeeText($comment->user->email);
            $response->assertSeeText($comment->image->name);
            $response->assertSeeText($comment->image->image);
        }
    }
}
