<?php

namespace Tests\Feature;

use App\Models\Image;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ImageTest extends TestCase
{
    /**
     * @group images
     * @return void
     */
    public function test_images_index()
    {
        $images = Image::all();
        $response = $this->getJson(route('images.index'));
        $response->assertStatus(200);

        foreach ($images as $image){
            $response->assertSeeText($image->name);
            $response->assertSeeText($image->image);
            $response->assertSeeText($image->user->name);
            $response->assertSeeText($image->user->email);
        }
    }
}
