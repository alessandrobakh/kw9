<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * @group users
     * @return void
     */
    public function test_users_index()
    {
        $users = User::all();
        $response = $this->getJson(route('users.index'));
        $response->assertStatus(200);

        foreach ($users as $user){
            $response->assertSeeText($user->name);
            $response->assertSeeText($user->email);
        }
    }
}
