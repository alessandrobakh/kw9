<?php

namespace Database\Factories;

use App\Models\Image as ImageAlias;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ImageAlias::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'image' => $this->getImage(rand(1,4))
        ];
    }

    private function getImage($image_number = 1)
    {
        $path = storage_path() . "/seed_images/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('images/' . $image_name, $resize->__toString());
        return 'images/' . $image_name;
    }
}
