<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Image;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = User::factory()->state([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
            'isAdmin' => true
        ])->create();
        User::factory()->count(4)->create();

        \App\Models\User::all()->each(function ($user) {
            $images_count = rand(5, 15);
            $comment_count = rand(3, 8);
            \App\Models\Image::factory()
                ->for(
                    $user
                )->has(
                    \App\Models\Comment::factory()
                        ->for(
                            \App\Models\User::where('id', '!=', $user->id)->first()
                        )->count($comment_count)
                )
                ->count($images_count)->create();
        });
    }
}
