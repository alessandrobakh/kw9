<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use App\Models\Image;
use Illuminate\Http\Request;

class CommentsController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param CommentRequest $request
     * @param Image $image
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(CommentRequest $request, Image $image)
    {
        $comment = new Comment($request->all());
        $comment->user()->associate($request->user());
        $comment->image_id = $image->id;
        $comment->save();
        return redirect()->back();
    }
}
