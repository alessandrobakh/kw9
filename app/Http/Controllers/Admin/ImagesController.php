<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImageRequest;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Auth::user());
        $images = Image::orderBy('id', 'desc')->paginate(9);
        return view('admin.images.index', compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('viewAny', Auth::user());
        return view('admin.images.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ImageRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(ImageRequest $request)
    {
        $image = new Image($request->all());
        $image->user()->associate($request->user());
        if ($request->hasFile('image')) {
            $image->image = $request->file('image')->store('images', 'public');
        }
        $image->save();
        $this->authorize('viewAny', Auth::user());
        return redirect()->route('admin.images.index')->with('status', 'Image created!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Image $image
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Image $image)
    {
        $this->authorize('viewAny', Auth::user());
        $comments = $image->comments->sortByDesc('id');
        return view('admin.images.show', compact('image', 'comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Image $image
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Image $image)
    {
        $this->authorize('viewAny', Auth::user());
        return view('admin.images.edit', compact('image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ImageRequest $request
     * @param \App\Models\Image $image
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(ImageRequest $request, Image $image)
    {
        $data = $request->all();
        if ($request->hasFile('image')){
            $path = $request->file('image')->store('images', 'public');
            $data['image'] = $path;
        }
        $image->user()->associate($request->user());
        $image->update($data);
        $this->authorize('viewAny', Auth::user());
        return redirect()->route('admin.images.index')->with('status', 'Image updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Image $image
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Image $image)
    {
        $this->authorize('viewAny', Auth::user());
        $image->delete();
        return redirect()->back()->with('status', 'Image deleted!');
    }
}
