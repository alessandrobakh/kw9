<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImageRequest;
use App\Models\Image;
use Illuminate\Http\Request;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::orderBy('id', 'desc')->paginate(9);
        return view('client.images.index', compact('images'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.images.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ImageRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(ImageRequest $request)
    {
        $image = new Image($request->all());
        $image->user()->associate($request->user());
        if ($request->hasFile('image')) {
            $image->image = $request->file('image')->store('images', 'public');
        }
        $image->save();
        $this->authorize('update', $image);
        return redirect()->route('client.images.index')->with('status', trans('messages.image_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Image  $image
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        $comments = $image->comments->sortByDesc('id');
        return view('client.images.show', compact('image', 'comments'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Image $image
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Image $image)
    {
        $this->authorize('forceDelete', $image);
        $image->delete();
        return redirect()->back()->with('status', trans('messages.image_deleted'));
    }
}
