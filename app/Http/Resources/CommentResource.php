<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'score' => $this->score,
            'body' => $this->body,
            'user' => new UserResource($this->user),
            'image' => new ImageResource($this->image),
        ];
    }
}
