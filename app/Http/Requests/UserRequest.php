<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $email_rules = is_null($this->user) ? 'required|email:rfc,dns|unique:users' :
            "required|email:rfc,dns|unique:users, {$this->user->id}";
        return [
            'name' => 'required|min:2',
            'email' => $email_rules,
            'password' => 'required|min:6'
        ];
    }
}
